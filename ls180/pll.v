// PLL simulation model; just connect input clock to output clock
module pll(ref_v, div_out_test, a0, a1, vco_test_ana, out_v);
  input a0;
  input a1;
  output div_out_test;
  output out_v;
  input ref_v;
  output vco_test_ana;

assign out_v = ref_v;
assign vco_test_ana = 0;
assign div_out_test = 0;

endmodule

