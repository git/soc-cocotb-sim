#!/usr/bin/env python3

"""builds nsxlib and other VHD files into object files using ghdl
"""

import os
import sys

SRC = [('nsxlib', 'vhd'),
       ('niolib', 'vhd'),
       ('vst_src', 'vst')]

# make and change to obj dir
os.system("mkdir -p obj")
cwd = os.getcwd()
os.chdir("obj")

for srcdir, suffix in SRC:
    # run ghdl -a on every vhd / vst file
    for fname in os.listdir("../%s" % srcdir):
        if not fname.endswith(".%s" % suffix):
            continue
        print (fname)
        prefix = fname[:-4] # strip ".vhd"
        os.system("ghdl -a -g --std=08 ../%s/%s" % (srcdir, fname))

# and chip and corona
#os.system("ghdl -a -g --std=08 ../chip_corona/chip_r.vhd")
#os.system("ghdl -a -g --std=08 ../chip_corona/corona_cts_r.vhd")

# back to original dir
os.chdir(cwd)
