#!/usr/bin/env python3

"""makes corrections to vst source from coriolis2 P&R
"""

import os
import sys

vhdl_header = """\
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

"""
# run through all files
for fname in os.listdir("vst_src"):
    if not fname.endswith(".vst"):
        continue
    print (fname)
    is_chip = fname.startswith("chip")
    # read the file
    fname = "vst_src/"+fname
    with open(fname) as f:
        txt = f.read()
    # replace vss / vdd : linkage bit with vss/vdd in bit
    txt = txt.replace("linkage bit", "in bit")
    # and double-underscores
    txt = txt.replace("__", "_")
    # special-case for chip.vst and chip_r.vst
    if is_chip:
        # add VHDL IEEE Library header
        txt = vhdl_header + txt
        # pad gpio fix
        txt = txt.replace("pad   : inout mux_bit bus",
                          "pad   : inout std_logic")
        # reset fix
        txt = txt.replace("sys_rst : gpio",
                          "p_sys_rst: gpio")

        # corona instance needs renaming too
        txt = txt.replace("corona : corona", "instance_corona : corona")

        # temporary hack to rename niolib to avoid name-clashes
        for cell in ['gpio', 'vss', 'vdd', 'iovss', 'iovdd']:
            txt = txt.replace(": %s" % cell, ": cmpt_%s" % cell)
            txt = txt.replace("component %s" % cell, "component cmpt_%s" % cell)
        # identify the chip ports and replace "in bit" with "inout std_logic"
        res = []
        found_chip = False
        done_chip = False
        for line in txt.splitlines():
            if done_chip:
                res.append(line)
                continue
            if not found_chip:
                if line.startswith("entity chip"):
                    found_chip = True
            else:
                is_power = False
                for port in ['vss', 'vdd', 'iovss', 'iovdd']:
                    if ' %s ' % port in line and 'in bit' in line:
                        is_power = True
                if not is_power:
                    # covers in bit_vector and out bit_vector as well
                    line = line.replace("in bit", "inout std_logic")
                    line = line.replace("out bit", "inout std_logic")
                done_chip = line.startswith("end chip")
            res.append(line)
        # re-join lines
        txt = '\n'.join(res)
        
    # write the file
    with open(fname, "w") as f:
        f.write(txt)
