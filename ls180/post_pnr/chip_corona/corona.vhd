
-- =======================================================================
-- Coriolis Structural VHDL Driver
-- Generated on Apr 10, 2021, 13:40
-- 
-- To be interoperable with Alliance, it uses it's special VHDL subset.
-- ("man vhdl" under Alliance for more informations)
-- =======================================================================

entity corona is
  port ( eint_0_from_pad              : in bit
       ; eint_1_from_pad              : in bit
       ; eint_2_from_pad              : in bit
       ; i2c_sda_i_from_pad           : in bit
       ; jtag_tck_from_pad            : in bit
       ; jtag_tdi_from_pad            : in bit
       ; jtag_tms_from_pad            : in bit
       ; spimaster_miso_from_pad      : in bit
       ; sys_clk_from_pad             : in bit
       ; sys_rst_from_pad             : in bit
       ; uart_rx_from_pad             : in bit
       ; uart_tx_from_pad             : in bit
       ; gpio_i_from_pad              : in bit_vector(15 downto 0)
       ; sdram_dq_i_from_pad          : in bit_vector(15 downto 0)
       ; nc_from_pad                  : in bit_vector(39 downto 0)
       ; eint_0_enable_to_pad         : out bit
       ; eint_1_enable_to_pad         : out bit
       ; eint_2_enable_to_pad         : out bit
       ; i2c_scl_enable_to_pad        : out bit
       ; i2c_scl_to_pad               : out bit
       ; i2c_sda_o_to_pad             : out bit
       ; i2c_sda_oe_to_pad            : out bit
       ; jtag_tck_enable_to_pad       : out bit
       ; jtag_tdi_enable_to_pad       : out bit
       ; jtag_tdo_enable_to_pad       : out bit
       ; jtag_tdo_to_pad              : out bit
       ; jtag_tms_enable_to_pad       : out bit
       ; nc_0_enable_to_pad           : out bit
       ; nc_10_enable_to_pad          : out bit
       ; nc_11_enable_to_pad          : out bit
       ; nc_12_enable_to_pad          : out bit
       ; nc_13_enable_to_pad          : out bit
       ; nc_14_enable_to_pad          : out bit
       ; nc_15_enable_to_pad          : out bit
       ; nc_16_enable_to_pad          : out bit
       ; nc_17_enable_to_pad          : out bit
       ; nc_18_enable_to_pad          : out bit
       ; nc_19_enable_to_pad          : out bit
       ; nc_1_enable_to_pad           : out bit
       ; nc_20_enable_to_pad          : out bit
       ; nc_21_enable_to_pad          : out bit
       ; nc_22_enable_to_pad          : out bit
       ; nc_23_enable_to_pad          : out bit
       ; nc_24_enable_to_pad          : out bit
       ; nc_25_enable_to_pad          : out bit
       ; nc_26_enable_to_pad          : out bit
       ; nc_27_enable_to_pad          : out bit
       ; nc_28_enable_to_pad          : out bit
       ; nc_29_enable_to_pad          : out bit
       ; nc_2_enable_to_pad           : out bit
       ; nc_30_enable_to_pad          : out bit
       ; nc_31_enable_to_pad          : out bit
       ; nc_32_enable_to_pad          : out bit
       ; nc_33_enable_to_pad          : out bit
       ; nc_34_enable_to_pad          : out bit
       ; nc_35_enable_to_pad          : out bit
       ; nc_36_enable_to_pad          : out bit
       ; nc_37_enable_to_pad          : out bit
       ; nc_38_enable_to_pad          : out bit
       ; nc_39_enable_to_pad          : out bit
       ; nc_3_enable_to_pad           : out bit
       ; nc_4_enable_to_pad           : out bit
       ; nc_5_enable_to_pad           : out bit
       ; nc_6_enable_to_pad           : out bit
       ; nc_7_enable_to_pad           : out bit
       ; nc_8_enable_to_pad           : out bit
       ; nc_9_enable_to_pad           : out bit
       ; sdram_a_0_enable_to_pad      : out bit
       ; sdram_a_10_enable_to_pad     : out bit
       ; sdram_a_11_enable_to_pad     : out bit
       ; sdram_a_12_enable_to_pad     : out bit
       ; sdram_a_1_enable_to_pad      : out bit
       ; sdram_a_2_enable_to_pad      : out bit
       ; sdram_a_3_enable_to_pad      : out bit
       ; sdram_a_4_enable_to_pad      : out bit
       ; sdram_a_5_enable_to_pad      : out bit
       ; sdram_a_6_enable_to_pad      : out bit
       ; sdram_a_7_enable_to_pad      : out bit
       ; sdram_a_8_enable_to_pad      : out bit
       ; sdram_a_9_enable_to_pad      : out bit
       ; sdram_ba_0_enable_to_pad     : out bit
       ; sdram_ba_1_enable_to_pad     : out bit
       ; sdram_cas_n_enable_to_pad    : out bit
       ; sdram_cas_n_to_pad           : out bit
       ; sdram_cke_enable_to_pad      : out bit
       ; sdram_cke_to_pad             : out bit
       ; sdram_clock_enable_to_pad    : out bit
       ; sdram_clock_to_pad           : out bit
       ; sdram_cs_n_enable_to_pad     : out bit
       ; sdram_cs_n_to_pad            : out bit
       ; sdram_dm_0_enable_to_pad     : out bit
       ; sdram_dm_1_enable_to_pad     : out bit
       ; sdram_ras_n_enable_to_pad    : out bit
       ; sdram_ras_n_to_pad           : out bit
       ; sdram_we_n_enable_to_pad     : out bit
       ; sdram_we_n_to_pad            : out bit
       ; spimaster_clk_enable_to_pad  : out bit
       ; spimaster_clk_to_pad         : out bit
       ; spimaster_cs_n_enable_to_pad : out bit
       ; spimaster_cs_n_to_pad        : out bit
       ; spimaster_miso_enable_to_pad : out bit
       ; spimaster_mosi_enable_to_pad : out bit
       ; spimaster_mosi_to_pad        : out bit
       ; sys_clk_enable_to_pad        : out bit
       ; sys_rst_enable_to_pad        : out bit
       ; uart_rx_enable_to_pad        : out bit
       ; uart_tx_enable_to_pad        : out bit
       ; sdram_ba_to_pad              : out bit_vector(1 downto 0)
       ; sdram_dm_to_pad              : out bit_vector(1 downto 0)
       ; sdram_a_to_pad               : out bit_vector(12 downto 0)
       ; gpio_o_to_pad                : out bit_vector(15 downto 0)
       ; gpio_oe_to_pad               : out bit_vector(15 downto 0)
       ; sdram_dq_o_to_pad            : out bit_vector(15 downto 0)
       ; sdram_dq_oe_to_pad           : out bit_vector(15 downto 0)
       ; vdd                          : linkage bit
       ; vss                          : linkage bit
       );
end corona;

architecture structural of corona is

  component ls180
    port ( eint_0                : in bit
         ; eint_1                : in bit
         ; eint_2                : in bit
         ; i2c_sda_i             : in bit
         ; jtag_tck              : in bit
         ; jtag_tdi              : in bit
         ; jtag_tms              : in bit
         ; spimaster_miso        : in bit
         ; sys_clk               : in bit
         ; sys_rst               : in bit
         ; uart_rx               : in bit
         ; uart_tx               : in bit
         ; gpio_i                : in bit_vector(15 downto 0)
         ; sdram_dq_i            : in bit_vector(15 downto 0)
         ; nc                    : in bit_vector(39 downto 0)
         ; eint_0_enable         : out bit
         ; eint_1_enable         : out bit
         ; eint_2_enable         : out bit
         ; i2c_scl               : out bit
         ; i2c_scl_enable        : out bit
         ; i2c_sda_o             : out bit
         ; i2c_sda_oe            : out bit
         ; jtag_tck_enable       : out bit
         ; jtag_tdi_enable       : out bit
         ; jtag_tdo              : out bit
         ; jtag_tdo_enable       : out bit
         ; jtag_tms_enable       : out bit
         ; nc_0_enable           : out bit
         ; nc_10_enable          : out bit
         ; nc_11_enable          : out bit
         ; nc_12_enable          : out bit
         ; nc_13_enable          : out bit
         ; nc_14_enable          : out bit
         ; nc_15_enable          : out bit
         ; nc_16_enable          : out bit
         ; nc_17_enable          : out bit
         ; nc_18_enable          : out bit
         ; nc_19_enable          : out bit
         ; nc_1_enable           : out bit
         ; nc_20_enable          : out bit
         ; nc_21_enable          : out bit
         ; nc_22_enable          : out bit
         ; nc_23_enable          : out bit
         ; nc_24_enable          : out bit
         ; nc_25_enable          : out bit
         ; nc_26_enable          : out bit
         ; nc_27_enable          : out bit
         ; nc_28_enable          : out bit
         ; nc_29_enable          : out bit
         ; nc_2_enable           : out bit
         ; nc_30_enable          : out bit
         ; nc_31_enable          : out bit
         ; nc_32_enable          : out bit
         ; nc_33_enable          : out bit
         ; nc_34_enable          : out bit
         ; nc_35_enable          : out bit
         ; nc_36_enable          : out bit
         ; nc_37_enable          : out bit
         ; nc_38_enable          : out bit
         ; nc_39_enable          : out bit
         ; nc_3_enable           : out bit
         ; nc_4_enable           : out bit
         ; nc_5_enable           : out bit
         ; nc_6_enable           : out bit
         ; nc_7_enable           : out bit
         ; nc_8_enable           : out bit
         ; nc_9_enable           : out bit
         ; sdram_a_0_enable      : out bit
         ; sdram_a_10_enable     : out bit
         ; sdram_a_11_enable     : out bit
         ; sdram_a_12_enable     : out bit
         ; sdram_a_1_enable      : out bit
         ; sdram_a_2_enable      : out bit
         ; sdram_a_3_enable      : out bit
         ; sdram_a_4_enable      : out bit
         ; sdram_a_5_enable      : out bit
         ; sdram_a_6_enable      : out bit
         ; sdram_a_7_enable      : out bit
         ; sdram_a_8_enable      : out bit
         ; sdram_a_9_enable      : out bit
         ; sdram_ba_0_enable     : out bit
         ; sdram_ba_1_enable     : out bit
         ; sdram_cas_n           : out bit
         ; sdram_cas_n_enable    : out bit
         ; sdram_cke             : out bit
         ; sdram_cke_enable      : out bit
         ; sdram_clock           : out bit
         ; sdram_clock_enable    : out bit
         ; sdram_cs_n            : out bit
         ; sdram_cs_n_enable     : out bit
         ; sdram_dm_0_enable     : out bit
         ; sdram_dm_1_enable     : out bit
         ; sdram_ras_n           : out bit
         ; sdram_ras_n_enable    : out bit
         ; sdram_we_n            : out bit
         ; sdram_we_n_enable     : out bit
         ; spimaster_clk         : out bit
         ; spimaster_clk_enable  : out bit
         ; spimaster_cs_n        : out bit
         ; spimaster_cs_n_enable : out bit
         ; spimaster_miso_enable : out bit
         ; spimaster_mosi        : out bit
         ; spimaster_mosi_enable : out bit
         ; sys_clk_enable        : out bit
         ; sys_rst_enable        : out bit
         ; uart_rx_enable        : out bit
         ; uart_tx_enable        : out bit
         ; sdram_ba              : out bit_vector(1 downto 0)
         ; sdram_dm              : out bit_vector(1 downto 0)
         ; sdram_a               : out bit_vector(12 downto 0)
         ; gpio_o                : out bit_vector(15 downto 0)
         ; gpio_oe               : out bit_vector(15 downto 0)
         ; sdram_dq_o            : out bit_vector(15 downto 0)
         ; sdram_dq_oe           : out bit_vector(15 downto 0)
         ; vdd                   : linkage bit
         ; vss                   : linkage bit
         );
  end component;



begin

  core : ls180
  port map ( eint_0                => eint_0_from_pad
           , eint_1                => eint_1_from_pad
           , eint_2                => eint_2_from_pad
           , i2c_sda_i             => i2c_sda_i_from_pad
           , jtag_tck              => jtag_tck_from_pad
           , jtag_tdi              => jtag_tdi_from_pad
           , jtag_tms              => jtag_tms_from_pad
           , spimaster_miso        => spimaster_miso_from_pad
           , sys_clk               => sys_clk_from_pad
           , sys_rst               => sys_rst_from_pad
           , uart_rx               => uart_rx_from_pad
           , uart_tx               => uart_tx_from_pad
           , gpio_i                => gpio_i_from_pad(15 downto 0)
           , sdram_dq_i            => sdram_dq_i_from_pad(15 downto 0)
           , nc                    => nc_from_pad(39 downto 0)
           , eint_0_enable         => eint_0_enable_to_pad
           , eint_1_enable         => eint_1_enable_to_pad
           , eint_2_enable         => eint_2_enable_to_pad
           , i2c_scl               => i2c_scl_to_pad
           , i2c_scl_enable        => i2c_scl_enable_to_pad
           , i2c_sda_o             => i2c_sda_o_to_pad
           , i2c_sda_oe            => i2c_sda_oe_to_pad
           , jtag_tck_enable       => jtag_tck_enable_to_pad
           , jtag_tdi_enable       => jtag_tdi_enable_to_pad
           , jtag_tdo              => jtag_tdo_to_pad
           , jtag_tdo_enable       => jtag_tdo_enable_to_pad
           , jtag_tms_enable       => jtag_tms_enable_to_pad
           , nc_0_enable           => nc_0_enable_to_pad
           , nc_10_enable          => nc_10_enable_to_pad
           , nc_11_enable          => nc_11_enable_to_pad
           , nc_12_enable          => nc_12_enable_to_pad
           , nc_13_enable          => nc_13_enable_to_pad
           , nc_14_enable          => nc_14_enable_to_pad
           , nc_15_enable          => nc_15_enable_to_pad
           , nc_16_enable          => nc_16_enable_to_pad
           , nc_17_enable          => nc_17_enable_to_pad
           , nc_18_enable          => nc_18_enable_to_pad
           , nc_19_enable          => nc_19_enable_to_pad
           , nc_1_enable           => nc_1_enable_to_pad
           , nc_20_enable          => nc_20_enable_to_pad
           , nc_21_enable          => nc_21_enable_to_pad
           , nc_22_enable          => nc_22_enable_to_pad
           , nc_23_enable          => nc_23_enable_to_pad
           , nc_24_enable          => nc_24_enable_to_pad
           , nc_25_enable          => nc_25_enable_to_pad
           , nc_26_enable          => nc_26_enable_to_pad
           , nc_27_enable          => nc_27_enable_to_pad
           , nc_28_enable          => nc_28_enable_to_pad
           , nc_29_enable          => nc_29_enable_to_pad
           , nc_2_enable           => nc_2_enable_to_pad
           , nc_30_enable          => nc_30_enable_to_pad
           , nc_31_enable          => nc_31_enable_to_pad
           , nc_32_enable          => nc_32_enable_to_pad
           , nc_33_enable          => nc_33_enable_to_pad
           , nc_34_enable          => nc_34_enable_to_pad
           , nc_35_enable          => nc_35_enable_to_pad
           , nc_36_enable          => nc_36_enable_to_pad
           , nc_37_enable          => nc_37_enable_to_pad
           , nc_38_enable          => nc_38_enable_to_pad
           , nc_39_enable          => nc_39_enable_to_pad
           , nc_3_enable           => nc_3_enable_to_pad
           , nc_4_enable           => nc_4_enable_to_pad
           , nc_5_enable           => nc_5_enable_to_pad
           , nc_6_enable           => nc_6_enable_to_pad
           , nc_7_enable           => nc_7_enable_to_pad
           , nc_8_enable           => nc_8_enable_to_pad
           , nc_9_enable           => nc_9_enable_to_pad
           , sdram_a_0_enable      => sdram_a_0_enable_to_pad
           , sdram_a_10_enable     => sdram_a_10_enable_to_pad
           , sdram_a_11_enable     => sdram_a_11_enable_to_pad
           , sdram_a_12_enable     => sdram_a_12_enable_to_pad
           , sdram_a_1_enable      => sdram_a_1_enable_to_pad
           , sdram_a_2_enable      => sdram_a_2_enable_to_pad
           , sdram_a_3_enable      => sdram_a_3_enable_to_pad
           , sdram_a_4_enable      => sdram_a_4_enable_to_pad
           , sdram_a_5_enable      => sdram_a_5_enable_to_pad
           , sdram_a_6_enable      => sdram_a_6_enable_to_pad
           , sdram_a_7_enable      => sdram_a_7_enable_to_pad
           , sdram_a_8_enable      => sdram_a_8_enable_to_pad
           , sdram_a_9_enable      => sdram_a_9_enable_to_pad
           , sdram_ba_0_enable     => sdram_ba_0_enable_to_pad
           , sdram_ba_1_enable     => sdram_ba_1_enable_to_pad
           , sdram_cas_n           => sdram_cas_n_to_pad
           , sdram_cas_n_enable    => sdram_cas_n_enable_to_pad
           , sdram_cke             => sdram_cke_to_pad
           , sdram_cke_enable      => sdram_cke_enable_to_pad
           , sdram_clock           => sdram_clock_to_pad
           , sdram_clock_enable    => sdram_clock_enable_to_pad
           , sdram_cs_n            => sdram_cs_n_to_pad
           , sdram_cs_n_enable     => sdram_cs_n_enable_to_pad
           , sdram_dm_0_enable     => sdram_dm_0_enable_to_pad
           , sdram_dm_1_enable     => sdram_dm_1_enable_to_pad
           , sdram_ras_n           => sdram_ras_n_to_pad
           , sdram_ras_n_enable    => sdram_ras_n_enable_to_pad
           , sdram_we_n            => sdram_we_n_to_pad
           , sdram_we_n_enable     => sdram_we_n_enable_to_pad
           , spimaster_clk         => spimaster_clk_to_pad
           , spimaster_clk_enable  => spimaster_clk_enable_to_pad
           , spimaster_cs_n        => spimaster_cs_n_to_pad
           , spimaster_cs_n_enable => spimaster_cs_n_enable_to_pad
           , spimaster_miso_enable => spimaster_miso_enable_to_pad
           , spimaster_mosi        => spimaster_mosi_to_pad
           , spimaster_mosi_enable => spimaster_mosi_enable_to_pad
           , sys_clk_enable        => sys_clk_enable_to_pad
           , sys_rst_enable        => sys_rst_enable_to_pad
           , uart_rx_enable        => uart_rx_enable_to_pad
           , uart_tx_enable        => uart_tx_enable_to_pad
           , sdram_ba              => sdram_ba_to_pad(1 downto 0)
           , sdram_dm              => sdram_dm_to_pad(1 downto 0)
           , sdram_a               => sdram_a_to_pad(12 downto 0)
           , gpio_o                => gpio_o_to_pad(15 downto 0)
           , gpio_oe               => gpio_oe_to_pad(15 downto 0)
           , sdram_dq_o            => sdram_dq_o_to_pad(15 downto 0)
           , sdram_dq_oe           => sdram_dq_oe_to_pad(15 downto 0)
           , vdd                   => vdd
           , vss                   => vss
           );

end structural;

