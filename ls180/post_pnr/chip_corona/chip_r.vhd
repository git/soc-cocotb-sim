LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

-- =======================================================================
-- Coriolis Structural VHDL Driver
-- Generated on Apr 10, 2021, 14:21
-- 
-- To be interoperable with Alliance, it uses it's special VHDL subset.
-- ("man vhdl" under Alliance for more informations)
-- =======================================================================

entity chip_r is
  port ( eint_0         : inout std_logic
       ; eint_1         : inout std_logic
       ; eint_2         : inout std_logic
       ; gpio_10        : inout std_logic
       ; gpio_11        : inout std_logic
       ; gpio_12        : inout std_logic
       ; gpio_13        : inout std_logic
       ; gpio_14        : inout std_logic
       ; gpio_15        : inout std_logic
       ; i2c_sda        : inout std_logic
       ; jtag_tck       : inout std_logic
       ; jtag_tdi       : inout std_logic
       ; jtag_tms       : inout std_logic
       ; sdram_dq_10    : inout std_logic
       ; sdram_dq_11    : inout std_logic
       ; sdram_dq_12    : inout std_logic
       ; sdram_dq_13    : inout std_logic
       ; sdram_dq_14    : inout std_logic
       ; sdram_dq_15    : inout std_logic
       ; spimaster_miso : inout std_logic
       ; sys_clk        : inout std_logic
       ; sys_rst        : inout std_logic
       ; uart_rx        : inout std_logic
       ; uart_tx        : inout std_logic
       ; nc             : inout std_logic_vector(39 downto 0)
       ; gpio_0         : inout std_logic
       ; gpio_1         : inout std_logic
       ; gpio_2         : inout std_logic
       ; gpio_3         : inout std_logic
       ; gpio_4         : inout std_logic
       ; gpio_5         : inout std_logic
       ; gpio_6         : inout std_logic
       ; gpio_7         : inout std_logic
       ; gpio_8         : inout std_logic
       ; gpio_9         : inout std_logic
       ; i2c_scl        : inout std_logic
       ; jtag_tdo       : inout std_logic
       ; sdram_cas_n    : inout std_logic
       ; sdram_cke      : inout std_logic
       ; sdram_clock    : inout std_logic
       ; sdram_cs_n     : inout std_logic
       ; sdram_dq_0     : inout std_logic
       ; sdram_dq_1     : inout std_logic
       ; sdram_dq_2     : inout std_logic
       ; sdram_dq_3     : inout std_logic
       ; sdram_dq_4     : inout std_logic
       ; sdram_dq_5     : inout std_logic
       ; sdram_dq_6     : inout std_logic
       ; sdram_dq_7     : inout std_logic
       ; sdram_dq_8     : inout std_logic
       ; sdram_dq_9     : inout std_logic
       ; sdram_ras_n    : inout std_logic
       ; sdram_we_n     : inout std_logic
       ; spimaster_clk  : inout std_logic
       ; spimaster_cs_n : inout std_logic
       ; spimaster_mosi : inout std_logic
       ; sdram_ba       : out std_logic_vector(1 downto 0)
       ; sdram_dm       : out std_logic_vector(1 downto 0)
       ; sdram_a        : out std_logic_vector(12 downto 0)
       ; iovdd          : in bit
       ; iovss          : in bit
       ; vdd            : in bit
       ; vss            : in bit
       );
end chip_r;

architecture structural of chip_r is

  component corona_cts_r
    port ( eint_0_from_pad              : in bit
         ; eint_1_from_pad              : in bit
         ; eint_2_from_pad              : in bit
         ; i2c_sda_i_from_pad           : in bit
         ; jtag_tck_from_pad            : in bit
         ; jtag_tdi_from_pad            : in bit
         ; jtag_tms_from_pad            : in bit
         ; spimaster_miso_from_pad      : in bit
         ; sys_clk_from_pad             : in bit
         ; sys_rst_from_pad             : in bit
         ; uart_rx_from_pad             : in bit
         ; uart_tx_from_pad             : in bit
         ; gpio_i_from_pad              : in bit_vector(15 downto 0)
         ; sdram_dq_i_from_pad          : in bit_vector(15 downto 0)
         ; nc_from_pad                  : in bit_vector(39 downto 0)
         ; eint_0_enable_to_pad         : out bit
         ; eint_1_enable_to_pad         : out bit
         ; eint_2_enable_to_pad         : out bit
         ; i2c_scl_enable_to_pad        : out bit
         ; i2c_scl_to_pad               : out bit
         ; i2c_sda_o_to_pad             : out bit
         ; i2c_sda_oe_to_pad            : out bit
         ; jtag_tck_enable_to_pad       : out bit
         ; jtag_tdi_enable_to_pad       : out bit
         ; jtag_tdo_enable_to_pad       : out bit
         ; jtag_tdo_to_pad              : out bit
         ; jtag_tms_enable_to_pad       : out bit
         ; nc_0_enable_to_pad           : out bit
         ; nc_10_enable_to_pad          : out bit
         ; nc_11_enable_to_pad          : out bit
         ; nc_12_enable_to_pad          : out bit
         ; nc_13_enable_to_pad          : out bit
         ; nc_14_enable_to_pad          : out bit
         ; nc_15_enable_to_pad          : out bit
         ; nc_16_enable_to_pad          : out bit
         ; nc_17_enable_to_pad          : out bit
         ; nc_18_enable_to_pad          : out bit
         ; nc_19_enable_to_pad          : out bit
         ; nc_1_enable_to_pad           : out bit
         ; nc_20_enable_to_pad          : out bit
         ; nc_21_enable_to_pad          : out bit
         ; nc_22_enable_to_pad          : out bit
         ; nc_23_enable_to_pad          : out bit
         ; nc_24_enable_to_pad          : out bit
         ; nc_25_enable_to_pad          : out bit
         ; nc_26_enable_to_pad          : out bit
         ; nc_27_enable_to_pad          : out bit
         ; nc_28_enable_to_pad          : out bit
         ; nc_29_enable_to_pad          : out bit
         ; nc_2_enable_to_pad           : out bit
         ; nc_30_enable_to_pad          : out bit
         ; nc_31_enable_to_pad          : out bit
         ; nc_32_enable_to_pad          : out bit
         ; nc_33_enable_to_pad          : out bit
         ; nc_34_enable_to_pad          : out bit
         ; nc_35_enable_to_pad          : out bit
         ; nc_36_enable_to_pad          : out bit
         ; nc_37_enable_to_pad          : out bit
         ; nc_38_enable_to_pad          : out bit
         ; nc_39_enable_to_pad          : out bit
         ; nc_3_enable_to_pad           : out bit
         ; nc_4_enable_to_pad           : out bit
         ; nc_5_enable_to_pad           : out bit
         ; nc_6_enable_to_pad           : out bit
         ; nc_7_enable_to_pad           : out bit
         ; nc_8_enable_to_pad           : out bit
         ; nc_9_enable_to_pad           : out bit
         ; sdram_a_0_enable_to_pad      : out bit
         ; sdram_a_10_enable_to_pad     : out bit
         ; sdram_a_11_enable_to_pad     : out bit
         ; sdram_a_12_enable_to_pad     : out bit
         ; sdram_a_1_enable_to_pad      : out bit
         ; sdram_a_2_enable_to_pad      : out bit
         ; sdram_a_3_enable_to_pad      : out bit
         ; sdram_a_4_enable_to_pad      : out bit
         ; sdram_a_5_enable_to_pad      : out bit
         ; sdram_a_6_enable_to_pad      : out bit
         ; sdram_a_7_enable_to_pad      : out bit
         ; sdram_a_8_enable_to_pad      : out bit
         ; sdram_a_9_enable_to_pad      : out bit
         ; sdram_ba_0_enable_to_pad     : out bit
         ; sdram_ba_1_enable_to_pad     : out bit
         ; sdram_cas_n_enable_to_pad    : out bit
         ; sdram_cas_n_to_pad           : out bit
         ; sdram_cke_enable_to_pad      : out bit
         ; sdram_cke_to_pad             : out bit
         ; sdram_clock_enable_to_pad    : out bit
         ; sdram_clock_to_pad           : out bit
         ; sdram_cs_n_enable_to_pad     : out bit
         ; sdram_cs_n_to_pad            : out bit
         ; sdram_dm_0_enable_to_pad     : out bit
         ; sdram_dm_1_enable_to_pad     : out bit
         ; sdram_ras_n_enable_to_pad    : out bit
         ; sdram_ras_n_to_pad           : out bit
         ; sdram_we_n_enable_to_pad     : out bit
         ; sdram_we_n_to_pad            : out bit
         ; spimaster_clk_enable_to_pad  : out bit
         ; spimaster_clk_to_pad         : out bit
         ; spimaster_cs_n_enable_to_pad : out bit
         ; spimaster_cs_n_to_pad        : out bit
         ; spimaster_miso_enable_to_pad : out bit
         ; spimaster_mosi_enable_to_pad : out bit
         ; spimaster_mosi_to_pad        : out bit
         ; sys_clk_enable_to_pad        : out bit
         ; sys_rst_enable_to_pad        : out bit
         ; uart_rx_enable_to_pad        : out bit
         ; uart_tx_enable_to_pad        : out bit
         ; sdram_ba_to_pad              : out bit_vector(1 downto 0)
         ; sdram_dm_to_pad              : out bit_vector(1 downto 0)
         ; sdram_a_to_pad               : out bit_vector(12 downto 0)
         ; gpio_o_to_pad                : out bit_vector(15 downto 0)
         ; gpio_oe_to_pad               : out bit_vector(15 downto 0)
         ; sdram_dq_o_to_pad            : out bit_vector(15 downto 0)
         ; sdram_dq_oe_to_pad           : out bit_vector(15 downto 0)
         ; vdd                          : in bit
         ; vss                          : in bit
         );
  end component;

  component cmpt_iovss
    port ( iovdd : in bit
         ; iovss : in bit
         ; vdd   : in bit
         ; vss   : in bit
         );
  end component;

  component cmpt_iovdd
    port ( iovdd : in bit
         ; iovss : in bit
         ; vdd   : in bit
         ; vss   : in bit
         );
  end component;

  component cmpt_vss
    port ( iovdd : in bit
         ; iovss : in bit
         ; vdd   : in bit
         ; vss   : in bit
         );
  end component;

  component cmpt_vdd
    port ( iovdd : in bit
         ; iovss : in bit
         ; vdd   : in bit
         ; vss   : in bit
         );
  end component;

  component cmpt_gpio
    port ( i     : in bit
         ; oe    : in bit
         ; o     : out bit
         ; pad   : inout std_logic
         ; iovdd : in bit
         ; iovss : in bit
         ; vdd   : in bit
         ; vss   : in bit
         );
  end component;

  signal chip_dummy_0                 :  bit;
  signal chip_dummy_1                 :  bit;
  signal chip_dummy_10                :  bit;
  signal chip_dummy_11                :  bit;
  signal chip_dummy_12                :  bit;
  signal chip_dummy_13                :  bit;
  signal chip_dummy_14                :  bit;
  signal chip_dummy_15                :  bit;
  signal chip_dummy_16                :  bit;
  signal chip_dummy_17                :  bit;
  signal chip_dummy_18                :  bit;
  signal chip_dummy_19                :  bit;
  signal chip_dummy_2                 :  bit;
  signal chip_dummy_20                :  bit;
  signal chip_dummy_21                :  bit;
  signal chip_dummy_22                :  bit;
  signal chip_dummy_23                :  bit;
  signal chip_dummy_24                :  bit;
  signal chip_dummy_25                :  bit;
  signal chip_dummy_26                :  bit;
  signal chip_dummy_27                :  bit;
  signal chip_dummy_28                :  bit;
  signal chip_dummy_29                :  bit;
  signal chip_dummy_3                 :  bit;
  signal chip_dummy_30                :  bit;
  signal chip_dummy_31                :  bit;
  signal chip_dummy_32                :  bit;
  signal chip_dummy_33                :  bit;
  signal chip_dummy_34                :  bit;
  signal chip_dummy_35                :  bit;
  signal chip_dummy_36                :  bit;
  signal chip_dummy_37                :  bit;
  signal chip_dummy_38                :  bit;
  signal chip_dummy_39                :  bit;
  signal chip_dummy_4                 :  bit;
  signal chip_dummy_40                :  bit;
  signal chip_dummy_41                :  bit;
  signal chip_dummy_42                :  bit;
  signal chip_dummy_43                :  bit;
  signal chip_dummy_44                :  bit;
  signal chip_dummy_45                :  bit;
  signal chip_dummy_46                :  bit;
  signal chip_dummy_47                :  bit;
  signal chip_dummy_48                :  bit;
  signal chip_dummy_49                :  bit;
  signal chip_dummy_5                 :  bit;
  signal chip_dummy_50                :  bit;
  signal chip_dummy_51                :  bit;
  signal chip_dummy_52                :  bit;
  signal chip_dummy_53                :  bit;
  signal chip_dummy_54                :  bit;
  signal chip_dummy_55                :  bit;
  signal chip_dummy_56                :  bit;
  signal chip_dummy_57                :  bit;
  signal chip_dummy_58                :  bit;
  signal chip_dummy_59                :  bit;
  signal chip_dummy_6                 :  bit;
  signal chip_dummy_60                :  bit;
  signal chip_dummy_61                :  bit;
  signal chip_dummy_62                :  bit;
  signal chip_dummy_63                :  bit;
  signal chip_dummy_64                :  bit;
  signal chip_dummy_65                :  bit;
  signal chip_dummy_66                :  bit;
  signal chip_dummy_67                :  bit;
  signal chip_dummy_68                :  bit;
  signal chip_dummy_69                :  bit;
  signal chip_dummy_7                 :  bit;
  signal chip_dummy_70                :  bit;
  signal chip_dummy_71                :  bit;
  signal chip_dummy_72                :  bit;
  signal chip_dummy_73                :  bit;
  signal chip_dummy_74                :  bit;
  signal chip_dummy_75                :  bit;
  signal chip_dummy_76                :  bit;
  signal chip_dummy_77                :  bit;
  signal chip_dummy_78                :  bit;
  signal chip_dummy_8                 :  bit;
  signal chip_dummy_9                 :  bit;
  signal eint_0_enable_to_pad         :  bit;
  signal eint_0_from_pad              :  bit;
  signal eint_1_enable_to_pad         :  bit;
  signal eint_1_from_pad              :  bit;
  signal eint_2_enable_to_pad         :  bit;
  signal eint_2_from_pad              :  bit;
  signal i2c_scl_enable_to_pad        :  bit;
  signal i2c_scl_to_pad               :  bit;
  signal i2c_sda_i_from_pad           :  bit;
  signal i2c_sda_o_to_pad             :  bit;
  signal i2c_sda_oe_to_pad            :  bit;
  signal jtag_tck_enable_to_pad       :  bit;
  signal jtag_tck_from_pad            :  bit;
  signal jtag_tdi_enable_to_pad       :  bit;
  signal jtag_tdi_from_pad            :  bit;
  signal jtag_tdo_enable_to_pad       :  bit;
  signal jtag_tdo_to_pad              :  bit;
  signal jtag_tms_enable_to_pad       :  bit;
  signal jtag_tms_from_pad            :  bit;
  signal nc_0_enable_to_pad           :  bit;
  signal nc_10_enable_to_pad          :  bit;
  signal nc_11_enable_to_pad          :  bit;
  signal nc_12_enable_to_pad          :  bit;
  signal nc_13_enable_to_pad          :  bit;
  signal nc_14_enable_to_pad          :  bit;
  signal nc_15_enable_to_pad          :  bit;
  signal nc_16_enable_to_pad          :  bit;
  signal nc_17_enable_to_pad          :  bit;
  signal nc_18_enable_to_pad          :  bit;
  signal nc_19_enable_to_pad          :  bit;
  signal nc_1_enable_to_pad           :  bit;
  signal nc_20_enable_to_pad          :  bit;
  signal nc_21_enable_to_pad          :  bit;
  signal nc_22_enable_to_pad          :  bit;
  signal nc_23_enable_to_pad          :  bit;
  signal nc_24_enable_to_pad          :  bit;
  signal nc_25_enable_to_pad          :  bit;
  signal nc_26_enable_to_pad          :  bit;
  signal nc_27_enable_to_pad          :  bit;
  signal nc_28_enable_to_pad          :  bit;
  signal nc_29_enable_to_pad          :  bit;
  signal nc_2_enable_to_pad           :  bit;
  signal nc_30_enable_to_pad          :  bit;
  signal nc_31_enable_to_pad          :  bit;
  signal nc_32_enable_to_pad          :  bit;
  signal nc_33_enable_to_pad          :  bit;
  signal nc_34_enable_to_pad          :  bit;
  signal nc_35_enable_to_pad          :  bit;
  signal nc_36_enable_to_pad          :  bit;
  signal nc_37_enable_to_pad          :  bit;
  signal nc_38_enable_to_pad          :  bit;
  signal nc_39_enable_to_pad          :  bit;
  signal nc_3_enable_to_pad           :  bit;
  signal nc_4_enable_to_pad           :  bit;
  signal nc_5_enable_to_pad           :  bit;
  signal nc_6_enable_to_pad           :  bit;
  signal nc_7_enable_to_pad           :  bit;
  signal nc_8_enable_to_pad           :  bit;
  signal nc_9_enable_to_pad           :  bit;
  signal sdram_a_0_enable_to_pad      :  bit;
  signal sdram_a_10_enable_to_pad     :  bit;
  signal sdram_a_11_enable_to_pad     :  bit;
  signal sdram_a_12_enable_to_pad     :  bit;
  signal sdram_a_1_enable_to_pad      :  bit;
  signal sdram_a_2_enable_to_pad      :  bit;
  signal sdram_a_3_enable_to_pad      :  bit;
  signal sdram_a_4_enable_to_pad      :  bit;
  signal sdram_a_5_enable_to_pad      :  bit;
  signal sdram_a_6_enable_to_pad      :  bit;
  signal sdram_a_7_enable_to_pad      :  bit;
  signal sdram_a_8_enable_to_pad      :  bit;
  signal sdram_a_9_enable_to_pad      :  bit;
  signal sdram_ba_0_enable_to_pad     :  bit;
  signal sdram_ba_1_enable_to_pad     :  bit;
  signal sdram_cas_n_enable_to_pad    :  bit;
  signal sdram_cas_n_to_pad           :  bit;
  signal sdram_cke_enable_to_pad      :  bit;
  signal sdram_cke_to_pad             :  bit;
  signal sdram_clock_enable_to_pad    :  bit;
  signal sdram_clock_to_pad           :  bit;
  signal sdram_cs_n_enable_to_pad     :  bit;
  signal sdram_cs_n_to_pad            :  bit;
  signal sdram_dm_0_enable_to_pad     :  bit;
  signal sdram_dm_1_enable_to_pad     :  bit;
  signal sdram_ras_n_enable_to_pad    :  bit;
  signal sdram_ras_n_to_pad           :  bit;
  signal sdram_we_n_enable_to_pad     :  bit;
  signal sdram_we_n_to_pad            :  bit;
  signal spimaster_clk_enable_to_pad  :  bit;
  signal spimaster_clk_to_pad         :  bit;
  signal spimaster_cs_n_enable_to_pad :  bit;
  signal spimaster_cs_n_to_pad        :  bit;
  signal spimaster_miso_enable_to_pad :  bit;
  signal spimaster_miso_from_pad      :  bit;
  signal spimaster_mosi_enable_to_pad :  bit;
  signal spimaster_mosi_to_pad        :  bit;
  signal sys_clk_enable_to_pad        :  bit;
  signal sys_clk_from_pad             :  bit;
  signal sys_rst_enable_to_pad        :  bit;
  signal sys_rst_from_pad             :  bit;
  signal uart_rx_enable_to_pad        :  bit;
  signal uart_rx_from_pad             :  bit;
  signal uart_tx_enable_to_pad        :  bit;
  signal uart_tx_from_pad             :  bit;
  signal sdram_ba_to_pad              :  bit_vector(1 downto 0);
  signal sdram_dm_to_pad              :  bit_vector(1 downto 0);
  signal sdram_a_to_pad               :  bit_vector(12 downto 0);
  signal gpio_i_from_pad              :  bit_vector(15 downto 0);
  signal gpio_o_to_pad                :  bit_vector(15 downto 0);
  signal gpio_oe_to_pad               :  bit_vector(15 downto 0);
  signal sdram_dq_i_from_pad          :  bit_vector(15 downto 0);
  signal sdram_dq_o_to_pad            :  bit_vector(15 downto 0);
  signal sdram_dq_oe_to_pad           :  bit_vector(15 downto 0);
  signal nc_from_pad                  :  bit_vector(39 downto 0);


begin

  p_sys_rst : cmpt_gpio
  port map ( i     => sys_rst_from_pad
           , oe    => sys_rst_enable_to_pad
           , o     => chip_dummy_73
           , pad   => sys_rst
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_gpio_15 : cmpt_gpio
  port map ( i     => gpio_o_to_pad(15)
           , oe    => gpio_oe_to_pad(15)
           , o     => gpio_i_from_pad(15)
           , pad   => gpio_15
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_gpio_14 : cmpt_gpio
  port map ( i     => gpio_o_to_pad(14)
           , oe    => gpio_oe_to_pad(14)
           , o     => gpio_i_from_pad(14)
           , pad   => gpio_14
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_gpio_13 : cmpt_gpio
  port map ( i     => gpio_o_to_pad(13)
           , oe    => gpio_oe_to_pad(13)
           , o     => gpio_i_from_pad(13)
           , pad   => gpio_13
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_gpio_12 : cmpt_gpio
  port map ( i     => gpio_o_to_pad(12)
           , oe    => gpio_oe_to_pad(12)
           , o     => gpio_i_from_pad(12)
           , pad   => gpio_12
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_gpio_11 : cmpt_gpio
  port map ( i     => gpio_o_to_pad(11)
           , oe    => gpio_oe_to_pad(11)
           , o     => gpio_i_from_pad(11)
           , pad   => gpio_11
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_gpio_10 : cmpt_gpio
  port map ( i     => gpio_o_to_pad(10)
           , oe    => gpio_oe_to_pad(10)
           , o     => gpio_i_from_pad(10)
           , pad   => gpio_10
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_dm_1 : cmpt_gpio
  port map ( i     => chip_dummy_3
           , oe    => sdram_dm_1_enable_to_pad
           , o     => sdram_dm_to_pad(1)
           , pad   => sdram_dm(1)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_dm_0 : cmpt_gpio
  port map ( i     => chip_dummy_40
           , oe    => sdram_dm_0_enable_to_pad
           , o     => sdram_dm_to_pad(0)
           , pad   => sdram_dm(0)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_39 : cmpt_gpio
  port map ( i     => nc_from_pad(39)
           , oe    => nc_39_enable_to_pad
           , o     => chip_dummy_78
           , pad   => nc(39)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_38 : cmpt_gpio
  port map ( i     => nc_from_pad(38)
           , oe    => nc_38_enable_to_pad
           , o     => chip_dummy_77
           , pad   => nc(38)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_37 : cmpt_gpio
  port map ( i     => nc_from_pad(37)
           , oe    => nc_37_enable_to_pad
           , o     => chip_dummy_76
           , pad   => nc(37)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_36 : cmpt_gpio
  port map ( i     => nc_from_pad(36)
           , oe    => nc_36_enable_to_pad
           , o     => chip_dummy_75
           , pad   => nc(36)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_35 : cmpt_gpio
  port map ( i     => nc_from_pad(35)
           , oe    => nc_35_enable_to_pad
           , o     => chip_dummy_74
           , pad   => nc(35)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_34 : cmpt_gpio
  port map ( i     => nc_from_pad(34)
           , oe    => nc_34_enable_to_pad
           , o     => chip_dummy_69
           , pad   => nc(34)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_33 : cmpt_gpio
  port map ( i     => nc_from_pad(33)
           , oe    => nc_33_enable_to_pad
           , o     => chip_dummy_64
           , pad   => nc(33)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_32 : cmpt_gpio
  port map ( i     => nc_from_pad(32)
           , oe    => nc_32_enable_to_pad
           , o     => chip_dummy_63
           , pad   => nc(32)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_31 : cmpt_gpio
  port map ( i     => nc_from_pad(31)
           , oe    => nc_31_enable_to_pad
           , o     => chip_dummy_62
           , pad   => nc(31)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_30 : cmpt_gpio
  port map ( i     => nc_from_pad(30)
           , oe    => nc_30_enable_to_pad
           , o     => chip_dummy_61
           , pad   => nc(30)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_dq_7 : cmpt_gpio
  port map ( i     => sdram_dq_o_to_pad(7)
           , oe    => sdram_dq_oe_to_pad(7)
           , o     => sdram_dq_i_from_pad(7)
           , pad   => sdram_dq_7
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_dq_9 : cmpt_gpio
  port map ( i     => sdram_dq_o_to_pad(9)
           , oe    => sdram_dq_oe_to_pad(9)
           , o     => sdram_dq_i_from_pad(9)
           , pad   => sdram_dq_9
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_dq_8 : cmpt_gpio
  port map ( i     => sdram_dq_o_to_pad(8)
           , oe    => sdram_dq_oe_to_pad(8)
           , o     => sdram_dq_i_from_pad(8)
           , pad   => sdram_dq_8
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_dq_0 : cmpt_gpio
  port map ( i     => sdram_dq_o_to_pad(0)
           , oe    => sdram_dq_oe_to_pad(0)
           , o     => sdram_dq_i_from_pad(0)
           , pad   => sdram_dq_0
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_dq_1 : cmpt_gpio
  port map ( i     => sdram_dq_o_to_pad(1)
           , oe    => sdram_dq_oe_to_pad(1)
           , o     => sdram_dq_i_from_pad(1)
           , pad   => sdram_dq_1
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_dq_2 : cmpt_gpio
  port map ( i     => sdram_dq_o_to_pad(2)
           , oe    => sdram_dq_oe_to_pad(2)
           , o     => sdram_dq_i_from_pad(2)
           , pad   => sdram_dq_2
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_dq_3 : cmpt_gpio
  port map ( i     => sdram_dq_o_to_pad(3)
           , oe    => sdram_dq_oe_to_pad(3)
           , o     => sdram_dq_i_from_pad(3)
           , pad   => sdram_dq_3
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_dq_4 : cmpt_gpio
  port map ( i     => sdram_dq_o_to_pad(4)
           , oe    => sdram_dq_oe_to_pad(4)
           , o     => sdram_dq_i_from_pad(4)
           , pad   => sdram_dq_4
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_dq_5 : cmpt_gpio
  port map ( i     => sdram_dq_o_to_pad(5)
           , oe    => sdram_dq_oe_to_pad(5)
           , o     => sdram_dq_i_from_pad(5)
           , pad   => sdram_dq_5
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_dq_6 : cmpt_gpio
  port map ( i     => sdram_dq_o_to_pad(6)
           , oe    => sdram_dq_oe_to_pad(6)
           , o     => sdram_dq_i_from_pad(6)
           , pad   => sdram_dq_6
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_uart_rx : cmpt_gpio
  port map ( i     => uart_rx_from_pad
           , oe    => uart_rx_enable_to_pad
           , o     => chip_dummy_71
           , pad   => uart_rx
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_spimaster_mosi : cmpt_gpio
  port map ( i     => chip_dummy_67
           , oe    => spimaster_mosi_enable_to_pad
           , o     => spimaster_mosi_to_pad
           , pad   => spimaster_mosi
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_ba_1 : cmpt_gpio
  port map ( i     => chip_dummy_52
           , oe    => sdram_ba_1_enable_to_pad
           , o     => sdram_ba_to_pad(1)
           , pad   => sdram_ba(1)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_ba_0 : cmpt_gpio
  port map ( i     => chip_dummy_51
           , oe    => sdram_ba_0_enable_to_pad
           , o     => sdram_ba_to_pad(0)
           , pad   => sdram_ba(0)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_i2c_scl : cmpt_gpio
  port map ( i     => chip_dummy_60
           , oe    => i2c_scl_enable_to_pad
           , o     => i2c_scl_to_pad
           , pad   => i2c_scl
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_vdd_4 : cmpt_vdd
  port map ( iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_vdd_1 : cmpt_vdd
  port map ( iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_vdd_0 : cmpt_vdd
  port map ( iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_vdd_2 : cmpt_vdd
  port map ( iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_vdd_3 : cmpt_vdd
  port map ( iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_cs_n : cmpt_gpio
  port map ( i     => chip_dummy_58
           , oe    => sdram_cs_n_enable_to_pad
           , o     => sdram_cs_n_to_pad
           , pad   => sdram_cs_n
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_iovss_0 : cmpt_iovss
  port map ( iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_iovss_2 : cmpt_iovss
  port map ( iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_iovss_1 : cmpt_iovss
  port map ( iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sys_clk : cmpt_gpio
  port map ( i     => sys_clk_from_pad
           , oe    => sys_clk_enable_to_pad
           , o     => chip_dummy_72
           , pad   => sys_clk
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_i2c_sda : cmpt_gpio
  port map ( i     => i2c_sda_o_to_pad
           , oe    => i2c_sda_oe_to_pad
           , o     => i2c_sda_i_from_pad
           , pad   => i2c_sda
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_a_10 : cmpt_gpio
  port map ( i     => chip_dummy_0
           , oe    => sdram_a_10_enable_to_pad
           , o     => sdram_a_to_pad(10)
           , pad   => sdram_a(10)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_a_11 : cmpt_gpio
  port map ( i     => chip_dummy_1
           , oe    => sdram_a_11_enable_to_pad
           , o     => sdram_a_to_pad(11)
           , pad   => sdram_a(11)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_a_12 : cmpt_gpio
  port map ( i     => chip_dummy_2
           , oe    => sdram_a_12_enable_to_pad
           , o     => sdram_a_to_pad(12)
           , pad   => sdram_a(12)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_uart_tx : cmpt_gpio
  port map ( i     => uart_tx_from_pad
           , oe    => uart_tx_enable_to_pad
           , o     => chip_dummy_70
           , pad   => uart_tx
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_0 : cmpt_gpio
  port map ( i     => nc_from_pad(0)
           , oe    => nc_0_enable_to_pad
           , o     => chip_dummy_4
           , pad   => nc(0)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_jtag_tck : cmpt_gpio
  port map ( i     => jtag_tck_from_pad
           , oe    => jtag_tck_enable_to_pad
           , o     => chip_dummy_8
           , pad   => jtag_tck
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_1 : cmpt_gpio
  port map ( i     => nc_from_pad(1)
           , oe    => nc_1_enable_to_pad
           , o     => chip_dummy_9
           , pad   => nc(1)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_2 : cmpt_gpio
  port map ( i     => nc_from_pad(2)
           , oe    => nc_2_enable_to_pad
           , o     => chip_dummy_10
           , pad   => nc(2)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_3 : cmpt_gpio
  port map ( i     => nc_from_pad(3)
           , oe    => nc_3_enable_to_pad
           , o     => chip_dummy_11
           , pad   => nc(3)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_4 : cmpt_gpio
  port map ( i     => nc_from_pad(4)
           , oe    => nc_4_enable_to_pad
           , o     => chip_dummy_12
           , pad   => nc(4)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_5 : cmpt_gpio
  port map ( i     => nc_from_pad(5)
           , oe    => nc_5_enable_to_pad
           , o     => chip_dummy_13
           , pad   => nc(5)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_6 : cmpt_gpio
  port map ( i     => nc_from_pad(6)
           , oe    => nc_6_enable_to_pad
           , o     => chip_dummy_17
           , pad   => nc(6)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_7 : cmpt_gpio
  port map ( i     => nc_from_pad(7)
           , oe    => nc_7_enable_to_pad
           , o     => chip_dummy_18
           , pad   => nc(7)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_8 : cmpt_gpio
  port map ( i     => nc_from_pad(8)
           , oe    => nc_8_enable_to_pad
           , o     => chip_dummy_19
           , pad   => nc(8)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_9 : cmpt_gpio
  port map ( i     => nc_from_pad(9)
           , oe    => nc_9_enable_to_pad
           , o     => chip_dummy_20
           , pad   => nc(9)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_ras_n : cmpt_gpio
  port map ( i     => chip_dummy_55
           , oe    => sdram_ras_n_enable_to_pad
           , o     => sdram_ras_n_to_pad
           , pad   => sdram_ras_n
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_jtag_tdo : cmpt_gpio
  port map ( i     => chip_dummy_7
           , oe    => jtag_tdo_enable_to_pad
           , o     => jtag_tdo_to_pad
           , pad   => jtag_tdo
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_jtag_tdi : cmpt_gpio
  port map ( i     => jtag_tdi_from_pad
           , oe    => jtag_tdi_enable_to_pad
           , o     => chip_dummy_6
           , pad   => jtag_tdi
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_vss_4 : cmpt_vss
  port map ( iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_vss_1 : cmpt_vss
  port map ( iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_vss_0 : cmpt_vss
  port map ( iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_vss_2 : cmpt_vss
  port map ( iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_vss_3 : cmpt_vss
  port map ( iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_spimaster_miso : cmpt_gpio
  port map ( i     => spimaster_miso_from_pad
           , oe    => spimaster_miso_enable_to_pad
           , o     => chip_dummy_68
           , pad   => spimaster_miso
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_spimaster_cs_n : cmpt_gpio
  port map ( i     => chip_dummy_66
           , oe    => spimaster_cs_n_enable_to_pad
           , o     => spimaster_cs_n_to_pad
           , pad   => spimaster_cs_n
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_spimaster_clk : cmpt_gpio
  port map ( i     => chip_dummy_65
           , oe    => spimaster_clk_enable_to_pad
           , o     => spimaster_clk_to_pad
           , pad   => spimaster_clk
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_we_n : cmpt_gpio
  port map ( i     => chip_dummy_57
           , oe    => sdram_we_n_enable_to_pad
           , o     => sdram_we_n_to_pad
           , pad   => sdram_we_n
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_a_6 : cmpt_gpio
  port map ( i     => chip_dummy_47
           , oe    => sdram_a_6_enable_to_pad
           , o     => sdram_a_to_pad(6)
           , pad   => sdram_a(6)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_a_5 : cmpt_gpio
  port map ( i     => chip_dummy_46
           , oe    => sdram_a_5_enable_to_pad
           , o     => sdram_a_to_pad(5)
           , pad   => sdram_a(5)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_a_4 : cmpt_gpio
  port map ( i     => chip_dummy_45
           , oe    => sdram_a_4_enable_to_pad
           , o     => sdram_a_to_pad(4)
           , pad   => sdram_a(4)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_a_3 : cmpt_gpio
  port map ( i     => chip_dummy_44
           , oe    => sdram_a_3_enable_to_pad
           , o     => sdram_a_to_pad(3)
           , pad   => sdram_a(3)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_a_2 : cmpt_gpio
  port map ( i     => chip_dummy_43
           , oe    => sdram_a_2_enable_to_pad
           , o     => sdram_a_to_pad(2)
           , pad   => sdram_a(2)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_a_1 : cmpt_gpio
  port map ( i     => chip_dummy_42
           , oe    => sdram_a_1_enable_to_pad
           , o     => sdram_a_to_pad(1)
           , pad   => sdram_a(1)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_a_0 : cmpt_gpio
  port map ( i     => chip_dummy_41
           , oe    => sdram_a_0_enable_to_pad
           , o     => sdram_a_to_pad(0)
           , pad   => sdram_a(0)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_a_9 : cmpt_gpio
  port map ( i     => chip_dummy_50
           , oe    => sdram_a_9_enable_to_pad
           , o     => sdram_a_to_pad(9)
           , pad   => sdram_a(9)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_a_8 : cmpt_gpio
  port map ( i     => chip_dummy_49
           , oe    => sdram_a_8_enable_to_pad
           , o     => sdram_a_to_pad(8)
           , pad   => sdram_a(8)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_a_7 : cmpt_gpio
  port map ( i     => chip_dummy_48
           , oe    => sdram_a_7_enable_to_pad
           , o     => sdram_a_to_pad(7)
           , pad   => sdram_a(7)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_jtag_tms : cmpt_gpio
  port map ( i     => jtag_tms_from_pad
           , oe    => jtag_tms_enable_to_pad
           , o     => chip_dummy_5
           , pad   => jtag_tms
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_cke : cmpt_gpio
  port map ( i     => chip_dummy_54
           , oe    => sdram_cke_enable_to_pad
           , o     => sdram_cke_to_pad
           , pad   => sdram_cke
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  corona : corona_cts_r
  port map ( eint_0_from_pad              => eint_0_from_pad
           , eint_1_from_pad              => eint_1_from_pad
           , eint_2_from_pad              => eint_2_from_pad
           , i2c_sda_i_from_pad           => i2c_sda_i_from_pad
           , jtag_tck_from_pad            => jtag_tck_from_pad
           , jtag_tdi_from_pad            => jtag_tdi_from_pad
           , jtag_tms_from_pad            => jtag_tms_from_pad
           , spimaster_miso_from_pad      => spimaster_miso_from_pad
           , sys_clk_from_pad             => sys_clk_from_pad
           , sys_rst_from_pad             => sys_rst_from_pad
           , uart_rx_from_pad             => uart_rx_from_pad
           , uart_tx_from_pad             => uart_tx_from_pad
           , gpio_i_from_pad              => gpio_i_from_pad(15 downto 0)
           , sdram_dq_i_from_pad          => sdram_dq_i_from_pad(15 downto 0)
           , nc_from_pad                  => nc_from_pad(39 downto 0)
           , eint_0_enable_to_pad         => eint_0_enable_to_pad
           , eint_1_enable_to_pad         => eint_1_enable_to_pad
           , eint_2_enable_to_pad         => eint_2_enable_to_pad
           , i2c_scl_enable_to_pad        => i2c_scl_enable_to_pad
           , i2c_scl_to_pad               => i2c_scl_to_pad
           , i2c_sda_o_to_pad             => i2c_sda_o_to_pad
           , i2c_sda_oe_to_pad            => i2c_sda_oe_to_pad
           , jtag_tck_enable_to_pad       => jtag_tck_enable_to_pad
           , jtag_tdi_enable_to_pad       => jtag_tdi_enable_to_pad
           , jtag_tdo_enable_to_pad       => jtag_tdo_enable_to_pad
           , jtag_tdo_to_pad              => jtag_tdo_to_pad
           , jtag_tms_enable_to_pad       => jtag_tms_enable_to_pad
           , nc_0_enable_to_pad           => nc_0_enable_to_pad
           , nc_10_enable_to_pad          => nc_10_enable_to_pad
           , nc_11_enable_to_pad          => nc_11_enable_to_pad
           , nc_12_enable_to_pad          => nc_12_enable_to_pad
           , nc_13_enable_to_pad          => nc_13_enable_to_pad
           , nc_14_enable_to_pad          => nc_14_enable_to_pad
           , nc_15_enable_to_pad          => nc_15_enable_to_pad
           , nc_16_enable_to_pad          => nc_16_enable_to_pad
           , nc_17_enable_to_pad          => nc_17_enable_to_pad
           , nc_18_enable_to_pad          => nc_18_enable_to_pad
           , nc_19_enable_to_pad          => nc_19_enable_to_pad
           , nc_1_enable_to_pad           => nc_1_enable_to_pad
           , nc_20_enable_to_pad          => nc_20_enable_to_pad
           , nc_21_enable_to_pad          => nc_21_enable_to_pad
           , nc_22_enable_to_pad          => nc_22_enable_to_pad
           , nc_23_enable_to_pad          => nc_23_enable_to_pad
           , nc_24_enable_to_pad          => nc_24_enable_to_pad
           , nc_25_enable_to_pad          => nc_25_enable_to_pad
           , nc_26_enable_to_pad          => nc_26_enable_to_pad
           , nc_27_enable_to_pad          => nc_27_enable_to_pad
           , nc_28_enable_to_pad          => nc_28_enable_to_pad
           , nc_29_enable_to_pad          => nc_29_enable_to_pad
           , nc_2_enable_to_pad           => nc_2_enable_to_pad
           , nc_30_enable_to_pad          => nc_30_enable_to_pad
           , nc_31_enable_to_pad          => nc_31_enable_to_pad
           , nc_32_enable_to_pad          => nc_32_enable_to_pad
           , nc_33_enable_to_pad          => nc_33_enable_to_pad
           , nc_34_enable_to_pad          => nc_34_enable_to_pad
           , nc_35_enable_to_pad          => nc_35_enable_to_pad
           , nc_36_enable_to_pad          => nc_36_enable_to_pad
           , nc_37_enable_to_pad          => nc_37_enable_to_pad
           , nc_38_enable_to_pad          => nc_38_enable_to_pad
           , nc_39_enable_to_pad          => nc_39_enable_to_pad
           , nc_3_enable_to_pad           => nc_3_enable_to_pad
           , nc_4_enable_to_pad           => nc_4_enable_to_pad
           , nc_5_enable_to_pad           => nc_5_enable_to_pad
           , nc_6_enable_to_pad           => nc_6_enable_to_pad
           , nc_7_enable_to_pad           => nc_7_enable_to_pad
           , nc_8_enable_to_pad           => nc_8_enable_to_pad
           , nc_9_enable_to_pad           => nc_9_enable_to_pad
           , sdram_a_0_enable_to_pad      => sdram_a_0_enable_to_pad
           , sdram_a_10_enable_to_pad     => sdram_a_10_enable_to_pad
           , sdram_a_11_enable_to_pad     => sdram_a_11_enable_to_pad
           , sdram_a_12_enable_to_pad     => sdram_a_12_enable_to_pad
           , sdram_a_1_enable_to_pad      => sdram_a_1_enable_to_pad
           , sdram_a_2_enable_to_pad      => sdram_a_2_enable_to_pad
           , sdram_a_3_enable_to_pad      => sdram_a_3_enable_to_pad
           , sdram_a_4_enable_to_pad      => sdram_a_4_enable_to_pad
           , sdram_a_5_enable_to_pad      => sdram_a_5_enable_to_pad
           , sdram_a_6_enable_to_pad      => sdram_a_6_enable_to_pad
           , sdram_a_7_enable_to_pad      => sdram_a_7_enable_to_pad
           , sdram_a_8_enable_to_pad      => sdram_a_8_enable_to_pad
           , sdram_a_9_enable_to_pad      => sdram_a_9_enable_to_pad
           , sdram_ba_0_enable_to_pad     => sdram_ba_0_enable_to_pad
           , sdram_ba_1_enable_to_pad     => sdram_ba_1_enable_to_pad
           , sdram_cas_n_enable_to_pad    => sdram_cas_n_enable_to_pad
           , sdram_cas_n_to_pad           => sdram_cas_n_to_pad
           , sdram_cke_enable_to_pad      => sdram_cke_enable_to_pad
           , sdram_cke_to_pad             => sdram_cke_to_pad
           , sdram_clock_enable_to_pad    => sdram_clock_enable_to_pad
           , sdram_clock_to_pad           => sdram_clock_to_pad
           , sdram_cs_n_enable_to_pad     => sdram_cs_n_enable_to_pad
           , sdram_cs_n_to_pad            => sdram_cs_n_to_pad
           , sdram_dm_0_enable_to_pad     => sdram_dm_0_enable_to_pad
           , sdram_dm_1_enable_to_pad     => sdram_dm_1_enable_to_pad
           , sdram_ras_n_enable_to_pad    => sdram_ras_n_enable_to_pad
           , sdram_ras_n_to_pad           => sdram_ras_n_to_pad
           , sdram_we_n_enable_to_pad     => sdram_we_n_enable_to_pad
           , sdram_we_n_to_pad            => sdram_we_n_to_pad
           , spimaster_clk_enable_to_pad  => spimaster_clk_enable_to_pad
           , spimaster_clk_to_pad         => spimaster_clk_to_pad
           , spimaster_cs_n_enable_to_pad => spimaster_cs_n_enable_to_pad
           , spimaster_cs_n_to_pad        => spimaster_cs_n_to_pad
           , spimaster_miso_enable_to_pad => spimaster_miso_enable_to_pad
           , spimaster_mosi_enable_to_pad => spimaster_mosi_enable_to_pad
           , spimaster_mosi_to_pad        => spimaster_mosi_to_pad
           , sys_clk_enable_to_pad        => sys_clk_enable_to_pad
           , sys_rst_enable_to_pad        => sys_rst_enable_to_pad
           , uart_rx_enable_to_pad        => uart_rx_enable_to_pad
           , uart_tx_enable_to_pad        => uart_tx_enable_to_pad
           , sdram_ba_to_pad              => sdram_ba_to_pad(1 downto 0)
           , sdram_dm_to_pad              => sdram_dm_to_pad(1 downto 0)
           , sdram_a_to_pad               => sdram_a_to_pad(12 downto 0)
           , gpio_o_to_pad                => gpio_o_to_pad(15 downto 0)
           , gpio_oe_to_pad               => gpio_oe_to_pad(15 downto 0)
           , sdram_dq_o_to_pad            => sdram_dq_o_to_pad(15 downto 0)
           , sdram_dq_oe_to_pad           => sdram_dq_oe_to_pad(15 downto 0)
           , vdd                          => vdd
           , vss                          => vss
           );

  p_gpio_7 : cmpt_gpio
  port map ( i     => gpio_o_to_pad(7)
           , oe    => gpio_oe_to_pad(7)
           , o     => gpio_i_from_pad(7)
           , pad   => gpio_7
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_gpio_6 : cmpt_gpio
  port map ( i     => gpio_o_to_pad(6)
           , oe    => gpio_oe_to_pad(6)
           , o     => gpio_i_from_pad(6)
           , pad   => gpio_6
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_gpio_5 : cmpt_gpio
  port map ( i     => gpio_o_to_pad(5)
           , oe    => gpio_oe_to_pad(5)
           , o     => gpio_i_from_pad(5)
           , pad   => gpio_5
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_gpio_4 : cmpt_gpio
  port map ( i     => gpio_o_to_pad(4)
           , oe    => gpio_oe_to_pad(4)
           , o     => gpio_i_from_pad(4)
           , pad   => gpio_4
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_gpio_3 : cmpt_gpio
  port map ( i     => gpio_o_to_pad(3)
           , oe    => gpio_oe_to_pad(3)
           , o     => gpio_i_from_pad(3)
           , pad   => gpio_3
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_gpio_2 : cmpt_gpio
  port map ( i     => gpio_o_to_pad(2)
           , oe    => gpio_oe_to_pad(2)
           , o     => gpio_i_from_pad(2)
           , pad   => gpio_2
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_gpio_1 : cmpt_gpio
  port map ( i     => gpio_o_to_pad(1)
           , oe    => gpio_oe_to_pad(1)
           , o     => gpio_i_from_pad(1)
           , pad   => gpio_1
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_gpio_0 : cmpt_gpio
  port map ( i     => gpio_o_to_pad(0)
           , oe    => gpio_oe_to_pad(0)
           , o     => gpio_i_from_pad(0)
           , pad   => gpio_0
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_clock : cmpt_gpio
  port map ( i     => chip_dummy_53
           , oe    => sdram_clock_enable_to_pad
           , o     => sdram_clock_to_pad
           , pad   => sdram_clock
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_gpio_9 : cmpt_gpio
  port map ( i     => gpio_o_to_pad(9)
           , oe    => gpio_oe_to_pad(9)
           , o     => gpio_i_from_pad(9)
           , pad   => gpio_9
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_gpio_8 : cmpt_gpio
  port map ( i     => gpio_o_to_pad(8)
           , oe    => gpio_oe_to_pad(8)
           , o     => gpio_i_from_pad(8)
           , pad   => gpio_8
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_dq_15 : cmpt_gpio
  port map ( i     => sdram_dq_o_to_pad(15)
           , oe    => sdram_dq_oe_to_pad(15)
           , o     => sdram_dq_i_from_pad(15)
           , pad   => sdram_dq_15
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_dq_14 : cmpt_gpio
  port map ( i     => sdram_dq_o_to_pad(14)
           , oe    => sdram_dq_oe_to_pad(14)
           , o     => sdram_dq_i_from_pad(14)
           , pad   => sdram_dq_14
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_dq_13 : cmpt_gpio
  port map ( i     => sdram_dq_o_to_pad(13)
           , oe    => sdram_dq_oe_to_pad(13)
           , o     => sdram_dq_i_from_pad(13)
           , pad   => sdram_dq_13
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_dq_12 : cmpt_gpio
  port map ( i     => sdram_dq_o_to_pad(12)
           , oe    => sdram_dq_oe_to_pad(12)
           , o     => sdram_dq_i_from_pad(12)
           , pad   => sdram_dq_12
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_dq_11 : cmpt_gpio
  port map ( i     => sdram_dq_o_to_pad(11)
           , oe    => sdram_dq_oe_to_pad(11)
           , o     => sdram_dq_i_from_pad(11)
           , pad   => sdram_dq_11
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_dq_10 : cmpt_gpio
  port map ( i     => sdram_dq_o_to_pad(10)
           , oe    => sdram_dq_oe_to_pad(10)
           , o     => sdram_dq_i_from_pad(10)
           , pad   => sdram_dq_10
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_eint_0 : cmpt_gpio
  port map ( i     => eint_0_from_pad
           , oe    => eint_0_enable_to_pad
           , o     => chip_dummy_14
           , pad   => eint_0
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_eint_1 : cmpt_gpio
  port map ( i     => eint_1_from_pad
           , oe    => eint_1_enable_to_pad
           , o     => chip_dummy_15
           , pad   => eint_1
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_eint_2 : cmpt_gpio
  port map ( i     => eint_2_from_pad
           , oe    => eint_2_enable_to_pad
           , o     => chip_dummy_16
           , pad   => eint_2
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_10 : cmpt_gpio
  port map ( i     => nc_from_pad(10)
           , oe    => nc_10_enable_to_pad
           , o     => chip_dummy_21
           , pad   => nc(10)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_11 : cmpt_gpio
  port map ( i     => nc_from_pad(11)
           , oe    => nc_11_enable_to_pad
           , o     => chip_dummy_22
           , pad   => nc(11)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_12 : cmpt_gpio
  port map ( i     => nc_from_pad(12)
           , oe    => nc_12_enable_to_pad
           , o     => chip_dummy_23
           , pad   => nc(12)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_13 : cmpt_gpio
  port map ( i     => nc_from_pad(13)
           , oe    => nc_13_enable_to_pad
           , o     => chip_dummy_24
           , pad   => nc(13)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_14 : cmpt_gpio
  port map ( i     => nc_from_pad(14)
           , oe    => nc_14_enable_to_pad
           , o     => chip_dummy_25
           , pad   => nc(14)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_15 : cmpt_gpio
  port map ( i     => nc_from_pad(15)
           , oe    => nc_15_enable_to_pad
           , o     => chip_dummy_26
           , pad   => nc(15)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_16 : cmpt_gpio
  port map ( i     => nc_from_pad(16)
           , oe    => nc_16_enable_to_pad
           , o     => chip_dummy_27
           , pad   => nc(16)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_17 : cmpt_gpio
  port map ( i     => nc_from_pad(17)
           , oe    => nc_17_enable_to_pad
           , o     => chip_dummy_28
           , pad   => nc(17)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_18 : cmpt_gpio
  port map ( i     => nc_from_pad(18)
           , oe    => nc_18_enable_to_pad
           , o     => chip_dummy_29
           , pad   => nc(18)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_19 : cmpt_gpio
  port map ( i     => nc_from_pad(19)
           , oe    => nc_19_enable_to_pad
           , o     => chip_dummy_30
           , pad   => nc(19)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_sdram_cas_n : cmpt_gpio
  port map ( i     => chip_dummy_56
           , oe    => sdram_cas_n_enable_to_pad
           , o     => sdram_cas_n_to_pad
           , pad   => sdram_cas_n
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_iovdd_0 : cmpt_iovdd
  port map ( iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_iovdd_2 : cmpt_iovdd
  port map ( iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  p_iovdd_1 : cmpt_iovdd
  port map ( iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_29 : cmpt_gpio
  port map ( i     => nc_from_pad(29)
           , oe    => nc_29_enable_to_pad
           , o     => chip_dummy_59
           , pad   => nc(29)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_20 : cmpt_gpio
  port map ( i     => nc_from_pad(20)
           , oe    => nc_20_enable_to_pad
           , o     => chip_dummy_31
           , pad   => nc(20)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_21 : cmpt_gpio
  port map ( i     => nc_from_pad(21)
           , oe    => nc_21_enable_to_pad
           , o     => chip_dummy_32
           , pad   => nc(21)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_22 : cmpt_gpio
  port map ( i     => nc_from_pad(22)
           , oe    => nc_22_enable_to_pad
           , o     => chip_dummy_33
           , pad   => nc(22)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_23 : cmpt_gpio
  port map ( i     => nc_from_pad(23)
           , oe    => nc_23_enable_to_pad
           , o     => chip_dummy_34
           , pad   => nc(23)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_24 : cmpt_gpio
  port map ( i     => nc_from_pad(24)
           , oe    => nc_24_enable_to_pad
           , o     => chip_dummy_35
           , pad   => nc(24)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_25 : cmpt_gpio
  port map ( i     => nc_from_pad(25)
           , oe    => nc_25_enable_to_pad
           , o     => chip_dummy_36
           , pad   => nc(25)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_26 : cmpt_gpio
  port map ( i     => nc_from_pad(26)
           , oe    => nc_26_enable_to_pad
           , o     => chip_dummy_37
           , pad   => nc(26)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_27 : cmpt_gpio
  port map ( i     => nc_from_pad(27)
           , oe    => nc_27_enable_to_pad
           , o     => chip_dummy_38
           , pad   => nc(27)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

  nc_28 : cmpt_gpio
  port map ( i     => nc_from_pad(28)
           , oe    => nc_28_enable_to_pad
           , o     => chip_dummy_39
           , pad   => nc(28)
           , iovdd => iovdd
           , iovss => iovss
           , vdd   => vdd
           , vss   => vss
           );

end structural;

